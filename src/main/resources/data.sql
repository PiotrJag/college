INSERT INTO users (`email`,`name`,`surname`,`password`,`role`) VALUES
('admin@gmail.com','piotr','jag','{bcrypt}$2a$10$NoYYHLuEh2R.WKymJpBSE.1U5get9hxHygqmU.VlD55Z8NUlTroK6','ADMIN'),
('student@gmail.com','jan','kowal','{bcrypt}$2a$10$rPFLvofzhQFYCEZ.lkWPvuJdUwaqJA0ZBVBrqz/1b2rkGT1Hg8lHK','STUDENT'),
('lecturer@gmail.com','jan','bo','{bcrypt}$2a$10$nBL9bLpX9iZT9yyT6u7RFuUiKi2O52ruZ6npss/zVC5KI4YcRfjFe','LECTURER');
