package pl.sdacademy.college.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name="lectures")
public class Lecture {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="student_group_id",nullable = false)
    private StudentGroup studentGroup;

    @ManyToOne
    @JoinColumn(name = "subject_id",nullable = false)
    private Subject subject;

    @ManyToOne
    @JoinColumn(name="user_id",nullable = false)
    private User user;

    @Column
    private LocalDate startDate;

    @Column
    private LocalDate endDate;
}
