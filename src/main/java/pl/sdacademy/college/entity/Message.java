package pl.sdacademy.college.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name="messages")
public class Message {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name ="sender_id")
    private User sender;

    @Column
    private String receiverMail;

    @Column
    private String text;

    @Column
    private String sendDate;
}
