package pl.sdacademy.college.entity;

public enum Role {
    STUDENT,
    LECTURER,
    ADMIN;
}
