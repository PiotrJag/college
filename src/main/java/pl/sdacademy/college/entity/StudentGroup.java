package pl.sdacademy.college.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name="student_groups")
public class StudentGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name; //infa12

    private String degree; //kierunek

    private String courseType; //stacjonarny/niestacjonarny enum

    private String year; //1

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany(mappedBy = "studentGroups", fetch = FetchType.EAGER)
    private Set<Subject> subjects;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany
    private Set<User> users;


//    @JoinColumn()
//    private Subject subject;

}

