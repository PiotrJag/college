package pl.sdacademy.college.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="student_index")
public class StudentIndex {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="subject_id")
    private Subject subject;

    @ManyToOne
    @JoinColumn(name="lecturer_id")
    private User lecturer;

    @ManyToOne
    @JoinColumn(name = "student_id")
    private User student;

    @Column
    private String grade;

    @ManyToOne
    @JoinColumn(name = "student_group_id")
    private StudentGroup studentGroup;

}
