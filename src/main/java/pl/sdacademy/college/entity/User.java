package pl.sdacademy.college.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String password;

    private String name;

    private String surname;

    @Column(nullable = false, unique = true)
    private String email;

    @Enumerated(EnumType.STRING)
    private Role role;

    @ManyToOne
    @JoinColumn(name="student_group_id")
    private StudentGroup studentGroup;

    private String country;

    private String postCode;

    private String city;

    private String street;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany
    private Set<StudentIndex> studentIndex;

}
