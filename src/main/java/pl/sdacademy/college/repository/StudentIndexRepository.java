package pl.sdacademy.college.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sdacademy.college.entity.StudentIndex;

import java.util.List;
import java.util.Optional;

public interface StudentIndexRepository extends JpaRepository<StudentIndex,Long> {

    List<StudentIndex> findAllByStudentId (Long id);

    Optional<StudentIndex> findByStudentId(Long id);

    Optional<StudentIndex> findByStudentIdAndSubjectId(Long studentId, Long subjectId);

    List<StudentIndex> findByStudentGroup_NameAndLecturer_EmailOrderBySubject(String name, String email);
}