package pl.sdacademy.college.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sdacademy.college.entity.Message;
import pl.sdacademy.college.entity.User;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message,Long> {

    List<Message> findAllByReceiverMail(String mail);
    List<Message> findAllBySender(User sender);
}
