package pl.sdacademy.college.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sdacademy.college.entity.Lecture;

public interface LectureRepository extends JpaRepository<Lecture,Long> {
}
