package pl.sdacademy.college.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sdacademy.college.entity.StudentGroup;

public interface StudentGroupRepository extends JpaRepository<StudentGroup,Long> {
}
