package pl.sdacademy.college.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sdacademy.college.entity.Role;
import pl.sdacademy.college.entity.User;


import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findAll();

    Optional<User> findByEmail(String email);

    Optional<User> findUserById(Long id);

    List<User> findAllByRole(Enum<Role> roleEnum);

    Optional<User> findUserByRoleAndId (Enum<Role> roleEnum, Long id);

}
