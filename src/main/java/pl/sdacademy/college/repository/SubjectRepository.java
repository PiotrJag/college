package pl.sdacademy.college.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sdacademy.college.entity.Subject;

import java.util.List;

public interface SubjectRepository extends JpaRepository<Subject,Long> {

    List<Subject> findAllByLecturerId (Long id);
}
