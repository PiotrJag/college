package pl.sdacademy.college.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sdacademy.college.component.StudentGroupMapper;
import pl.sdacademy.college.dto.StudentGroupDto;
import pl.sdacademy.college.dto.UserDto;
import pl.sdacademy.college.entity.StudentGroup;
import pl.sdacademy.college.entity.Subject;
import pl.sdacademy.college.entity.User;
import pl.sdacademy.college.exception.EntityNotFoundException;
import pl.sdacademy.college.repository.StudentGroupRepository;
import pl.sdacademy.college.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class StudentGroupService {

    private final StudentGroupRepository studentGroupRepository;
    private final StudentGroupMapper studentGroupMapper;
    private final UserRepository userRepository;

    public StudentGroupDto findById(Long id){
        return studentGroupRepository.findById(id)
                .map(studentGroupMapper::map)
                .orElseThrow(()->new EntityNotFoundException(StudentGroup.class,id));
    }

    public StudentGroup findGroupById(Long id){
        return studentGroupRepository.findById(id)
                .orElseThrow(()->new EntityNotFoundException(StudentGroup.class,id));
    }

    public List<StudentGroupDto> findAll(){
       return studentGroupRepository.findAll()
               .stream()
               .map(studentGroupMapper::map)
               .collect(Collectors.toList());
    }

    @Transactional
    public void deleteById(Long id){
        studentGroupRepository.deleteById(id);
    }

    @Transactional
    public void save(StudentGroupDto dto){
        studentGroupRepository.save(studentGroupMapper.map(dto));
    }

    @Transactional
    public void update(StudentGroupDto dto,Long id){
        StudentGroup entity = studentGroupRepository
                .findById(id)
                .orElseThrow(()-> new EntityNotFoundException(StudentGroup.class,id));
        entity.setId(dto.getId());
        entity.setCourseType(dto.getCourseType());
        entity.setDegree(dto.getDegree());
        entity.setName(dto.getName());
        entity.setYear(dto.getYear());
    }

    public void addGroupToUser(User user, Long groupId) {
        Optional<StudentGroup> optionalStudentGroup = studentGroupRepository.findById(groupId);
        if (optionalStudentGroup.isPresent()) {
            StudentGroup studentGroup = optionalStudentGroup.get();

            studentGroup.getUsers().add(user);
            studentGroupRepository.save(studentGroup);

            user.setStudentGroup(studentGroup);
            userRepository.save(user);
        }
    }

    public void removeGroupFromUser(User user, Long groupId) {
        Optional<StudentGroup> optionalStudentGroup = studentGroupRepository.findById(groupId);
        if (optionalStudentGroup.isPresent()) {
            StudentGroup studentGroup = optionalStudentGroup.get();

            studentGroup.getUsers().remove(user);
            studentGroupRepository.save(studentGroup);

            user.setStudentGroup(null);
            userRepository.save(user);
        }
    }
}
