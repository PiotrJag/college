package pl.sdacademy.college.service;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sdacademy.college.component.MessageMapper;
import pl.sdacademy.college.component.UserMapper;
import pl.sdacademy.college.dto.MessageDto;
import pl.sdacademy.college.entity.Message;
import pl.sdacademy.college.exception.EntityNotFoundException;
import pl.sdacademy.college.repository.MessageRepository;

import javax.transaction.Transactional;
import java.util.List;

@RequiredArgsConstructor
@Service
public class MessageService {

    private final MessageRepository messageRepository;
    private final MessageMapper messageMapper;
    private final UserService userService;
    private final UserMapper userMapper;

    @Transactional
    public void save(MessageDto messageDto){
        messageRepository.save(messageMapper.map(messageDto));
    }

    public List<Message> findMessagesByUserId(Long id){
        return messageRepository.findAllByReceiverMail(userService.findUserById(id).getEmail());
    }
    public List<Message> findMessagesByReceiverUserMail(String mail){
        return messageRepository.findAllByReceiverMail(mail);
    }

    public List<Message> findMessagesBySenderUserMail(String mail){
        return messageRepository.findAllBySender(userMapper.map(userService.findUserByEmail(mail)));
    }
    public MessageDto findMessageById(Long id) {
        return messageRepository.findById(id).map(messageMapper::map).orElseThrow(()-> new EntityNotFoundException(Message.class,id));
    }
}
