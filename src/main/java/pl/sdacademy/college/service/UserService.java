package pl.sdacademy.college.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sdacademy.college.component.UserMapper;
import pl.sdacademy.college.dto.UserDto;
import pl.sdacademy.college.entity.Role;
import pl.sdacademy.college.entity.StudentIndex;
import pl.sdacademy.college.entity.Subject;
import pl.sdacademy.college.entity.User;
import pl.sdacademy.college.exception.EntityNotFoundByMailException;
import pl.sdacademy.college.exception.EntityNotFoundException;
import pl.sdacademy.college.repository.StudentGroupRepository;
import pl.sdacademy.college.repository.StudentIndexRepository;
import pl.sdacademy.college.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;
    private final StudentIndexRepository studentIndexRepository;


    public UserDto findUserByRoleAndId(Enum<Role> roleEnum,Long id){
        return userRepository.findUserByRoleAndId(roleEnum,id)
                .map(userMapper::map)
                .orElseThrow(()->new EntityNotFoundException(User.class,id));
    }


    public UserDto findUserByEmail(String email){
        return userRepository.findByEmail(email)
                .map(userMapper::map)
                .orElseThrow(()->new EntityNotFoundByMailException(User.class,email));
    }

    public List<User> findUsersByRole(Role role){
        return userRepository.findAllByRole(role);
    }

    public List<UserDto> findAll() {
        return userRepository.findAll().stream()
                .map(userMapper::map).collect(Collectors.toList());
    }

    public UserDto findById(Long id) {
        return userRepository.findUserById(id)
                .map(userMapper::map)
                .orElseThrow(() -> new EntityNotFoundException(User.class, id));
    }

    public User findUserById(Long id) {
        return userRepository.findUserById(id)
                .orElseThrow(() -> new EntityNotFoundException(User.class, id));
    }

    @Transactional
    public UserDto save(UserDto user) {
        if (user.getId() != null) {
            return update(user.getId(), user);
        }
        User entity = userMapper.map(user);
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        entity.setPassword(encodedPassword);
        return userMapper.map(userRepository.save(entity));
    }

    @Transactional
    public UserDto update(Long id, UserDto user){
        User entity = findEntity(id);
        entity.setEmail(user.getEmail());
        entity.setName(user.getName());
        entity.setSurname(user.getSurname());
        entity.setRole(user.getRole());
        entity.setCountry(user.getCountry());
        entity.setPostCode(user.getPostCode());
        entity.setCity(user.getCity());
        entity.setStreet(user.getStreet());
//        entity.setStudentIndex(user.getStudentIndex());
        entity.setStudentGroup(user.getStudentGroup());
        return userMapper.map(userRepository.save(entity));
    }

    @Transactional
    public void delete(Long id) {
        User entity = findEntity(id);
        userRepository.delete(entity);
    }

    private User findEntity(Long id) {
        return userRepository.findUserById(id)
                .orElseThrow(() -> new EntityNotFoundException(User.class, id));
    }

    public void createIndexForUser(User user, Set<Subject> subjects) {
        for (Subject subject: subjects) {

            StudentIndex studentIndex = new StudentIndex();
            studentIndex.setStudent(user);
            studentIndex.setSubject(subject);
            studentIndex.setLecturer(subject.getLecturer());
            studentIndex.setStudentGroup(user.getStudentGroup());
            studentIndexRepository.save(studentIndex);

            user.getStudentIndex().add(studentIndex);
            userRepository.save(user);
        }

    }

    public void removeIndexFromUser(User user, Set<Subject> subjects) {
        for (Subject subject: subjects) {
            Optional<StudentIndex> optionalStudentIndex = studentIndexRepository.findByStudentIdAndSubjectId(user.getId(), subject.getId());
            if (optionalStudentIndex.isPresent()) {
                StudentIndex studentIndex = optionalStudentIndex.get();

                user.getStudentIndex().remove(studentIndex);
                userRepository.save(user);

                studentIndexRepository.delete(studentIndex);
            }
        }
    }
}