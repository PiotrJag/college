package pl.sdacademy.college.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sdacademy.college.component.SubjectMapper;
import pl.sdacademy.college.dto.SubjectDto;
import pl.sdacademy.college.entity.StudentGroup;
import pl.sdacademy.college.entity.Subject;
import pl.sdacademy.college.entity.User;
import pl.sdacademy.college.exception.EntityNotFoundException;
import pl.sdacademy.college.repository.StudentGroupRepository;
import pl.sdacademy.college.repository.SubjectRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class SubjectService {

    private final SubjectRepository subjectRepository;
    private final StudentGroupRepository studentGroupRepository;
    private final SubjectMapper subjectMapper;


    public List<SubjectDto> findSubjectsByLecturerId(Long id){
        return subjectRepository.findAllByLecturerId(id)
                .stream()
                .map(subjectMapper::map)
                .collect(Collectors.toList());
    }

    public List<SubjectDto> findAll() {
        return subjectRepository.findAll()
                .stream()
                .map(subjectMapper::map)
                .collect(Collectors.toList());
    }

    public SubjectDto findById(Long id) {
        return subjectRepository.findById(id)
                .map(subjectMapper::map)
                .orElseThrow(() -> new EntityNotFoundException(SubjectDto.class, id));
    }


    public Subject findGroupById(Long id) {
        return subjectRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(SubjectDto.class, id));
    }

    public void addLecturerToSubject(Long subjectId, User lecturer) {
        Optional<Subject> optionalSubject = subjectRepository.findById(subjectId);
        if (optionalSubject.isPresent()) {
            Subject subject = optionalSubject.get();
            subject.setLecturer(lecturer);
            subjectRepository.save(subject);
        }
    }

    public void removeLecturerFromSubject (Long subjectId, User lecturer){
            Optional<Subject> optionalSubject = subjectRepository.findById(subjectId);
            if (optionalSubject.isPresent()&&lecturer.equals(optionalSubject.get().getLecturer())) {
                Subject subject = optionalSubject.get();
                subject.setLecturer(null);
                subjectRepository.save(subject);
            }
        }


    @Transactional
    public void save(SubjectDto subjectDto) {
        subjectRepository.save(subjectMapper.map(subjectDto));
    }

    @Transactional
    public void update(SubjectDto dto) {
        Subject entity = new Subject();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setStudentGroups(dto.getStudentGroups());
    }

    @Transactional
    public void deleteById(Long id) {
        subjectRepository.deleteById(id);
    }


    public void addSubjectToGroup(StudentGroup group, Long subjectId) {
        Optional<Subject> optionalSubject = subjectRepository.findById(subjectId);
        if (optionalSubject.isPresent()) {
            Subject subject = optionalSubject.get();

            subject.getStudentGroups().add(group);
            subjectRepository.save(subject);

            group.getSubjects().add(subject);
            studentGroupRepository.save(group);
        }
    }

    public void removeSubjectFromGroup(StudentGroup group, Long subjectId) {
        Optional<Subject> optionalSubject = subjectRepository.findById(subjectId);
        if (optionalSubject.isPresent()) {
            Subject subject = optionalSubject.get();

            subject.getStudentGroups().remove(group);
            subjectRepository.save(subject);

            group.getSubjects().remove(subject);
            studentGroupRepository.save(group);
        }
    }


}
