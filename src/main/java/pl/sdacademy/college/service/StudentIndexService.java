package pl.sdacademy.college.service;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sdacademy.college.component.StudentIndexMapper;
import pl.sdacademy.college.dto.StudentIndexDto;
import pl.sdacademy.college.entity.StudentIndex;
import pl.sdacademy.college.exception.EntityNotFoundException;
import pl.sdacademy.college.repository.StudentIndexRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class StudentIndexService {

    private final StudentIndexRepository studentIndexRepository;
    private final StudentIndexMapper studentIndexMapper;

    public List<StudentIndexDto> findAll(){
        return studentIndexRepository
                .findAll()
                .stream()
                .map(studentIndexMapper::map)
                .collect(Collectors.toList());
    }

    public List<StudentIndexDto>findByStudentId(Long id){
    return studentIndexRepository
            .findAllByStudentId(id)
            .stream()
            .map(studentIndexMapper::map)
            .collect(Collectors.toList());

    }

    public StudentIndexDto findById(Long id){
        return studentIndexRepository
                .findById(id)
                .map(studentIndexMapper::map)
                .orElseThrow(()-> new EntityNotFoundException(StudentIndex.class,id));
    }

    @Transactional
    public StudentIndexDto save(StudentIndexDto studentIndexDto){
        if (studentIndexDto.getId() != null) {
            return update(studentIndexDto.getId(), studentIndexDto);
        }
        StudentIndex entity = studentIndexMapper.map(studentIndexDto);
        return studentIndexMapper.map(studentIndexRepository.save(entity));
    }

    @Transactional
    public void deleteById(Long id){
        studentIndexRepository.deleteById(id);
    }

    @Transactional
    public StudentIndexDto update(Long id,StudentIndexDto dto){
        StudentIndex entity = studentIndexRepository
                .findById(id)
                .orElseThrow(()->new EntityNotFoundException(StudentIndex.class,id));
        entity.setSubject(dto.getSubject());
        entity.setLecturer(dto.getLecturer());
//        entity.setId(dto.getId());
        entity.setGrade(dto.getGrade());
        entity.setStudent(dto.getStudent());
        return studentIndexMapper.map(studentIndexRepository.save(entity));
    }

    @Transactional
    public StudentIndexDto updateGrade(Long id, String grade) {
        StudentIndex entity = studentIndexRepository
                .findById(id)
                .orElseThrow(()->new EntityNotFoundException(StudentIndex.class,id));
        entity.setGrade(grade);
        return studentIndexMapper.map(studentIndexRepository.save(entity));

    }

    public List<StudentIndex> findIndex(String nameGroup, String emailLecturer) {
        return studentIndexRepository.findByStudentGroup_NameAndLecturer_EmailOrderBySubject(nameGroup, emailLecturer);

    }
}
