package pl.sdacademy.college.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sdacademy.college.dto.LectureDto;
import pl.sdacademy.college.entity.Lecture;
import pl.sdacademy.college.exception.EntityNotFoundException;
import pl.sdacademy.college.repository.LectureRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;


@RequiredArgsConstructor
@Service
public class LectureService {

    private final LectureRepository lectureRepository;


    public List<LectureDto> findAll(){
        return lectureRepository.findAll()
        .stream()
        .map(this::map)
        .collect(Collectors.toList());
    }

    public LectureDto findById(long id){
        return lectureRepository.findById(id)
                .map(this::map)
                .orElseThrow(()-> new EntityNotFoundException(Lecture.class,id));
    }

    @Transactional
    public void deleteById(long id){
        lectureRepository.deleteById(id);
    }

    @Transactional
    public void save(LectureDto dto){
        lectureRepository.save(map(dto));
    }

    @Transactional
    public void update(LectureDto dto,Long id){
        Lecture entity = lectureRepository
                .findById(id)
                .orElseThrow(()-> new EntityNotFoundException(Lecture.class,id));
        entity.setId(dto.getId());
        entity.setStudentGroup(dto.getStudentGroup());
        entity.setStartDate(dto.getStartDate());
        entity.setEndDate(dto.getEndDate());
    }


    private LectureDto map(Lecture entity){
        LectureDto dto = new LectureDto();
        dto.setId(entity.getId());
        dto.setStudentGroup(entity.getStudentGroup());
        dto.setSubject(entity.getSubject());
        dto.setEndDate(entity.getEndDate());
        return dto;
    }

    private Lecture map(LectureDto dto){
        Lecture entity = new Lecture();
        entity.setId(dto.getId());
        entity.setStudentGroup(dto.getStudentGroup());
        entity.setStartDate(dto.getStartDate());
        entity.setEndDate(dto.getEndDate());
        return entity;
    }
}
