package pl.sdacademy.college.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.sdacademy.college.component.MessageMapper;
import pl.sdacademy.college.dto.*;
import pl.sdacademy.college.entity.StudentIndex;
import pl.sdacademy.college.service.MessageService;
import pl.sdacademy.college.service.StudentGroupService;
import pl.sdacademy.college.service.StudentIndexService;
import pl.sdacademy.college.service.UserService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Controller
@RequestMapping("/lecturer")
public class LecturerController {

    private final StudentGroupService studentGroupService;
    private final StudentIndexService studentIndexService;
    private final UserService userService;
    private final MessageService messageService;
    private final MessageMapper messageMapper;


    @GetMapping
    public String lecturerForm(Model model, Principal principal) {
        model.addAttribute("name", principal.getName());
        return "lecturer";
    }

    @PostMapping("/group_grades_submition")
    public String groupGradesSubmition(StudentGradesDto grades) {

        for (StudentGradeDto grade: grades.getGrades()) {
            if (!grade.getGrade().isEmpty()) {
                studentIndexService.updateGrade(grade.getStudentIndex(), grade.getGrade());
            }
        }

        return "redirect:/lecturer/index_list";
    }

    @GetMapping("/index_list")
    public String getIndexList(Model model, Principal principal) {

        List<StudentGroupDto> studentGroupList = studentGroupService.findAll();
        model.addAttribute("student_group_list", studentGroupList);

        return "lecturer/index_list";
    }

    @PostMapping("/index_list")
    public String postIndexList(Model model, @Valid @ModelAttribute("student_group") StudentGroupDto newStudentGroup,
                                @Valid @ModelAttribute("student_index") List<StudentIndexDto> newStudentIndexList,
                                BindingResult errors,
                                Principal principal) {
        if (errors.hasErrors()) {
            return "lecturer/index_list";
        }

        List<StudentGroupDto> studentGroupList = studentGroupService.findAll();
        model.addAttribute("student_group_list", studentGroupList);

        List<StudentIndex> studentIndexList = studentIndexService.findIndex(newStudentGroup.getName(), principal.getName());
        model.addAttribute("index_list", studentIndexList);
        model.addAttribute("students_grades", new StudentGradesDto(studentIndexList));

        return "lecturer/index_list";
    }

    @ModelAttribute("student_group")
    public StudentGroupDto studentGroupDto() {
        return new StudentGroupDto();
    }

    @ModelAttribute("student_index")
    public StudentIndexDto studentIndexDto() {
        return new StudentIndexDto();
    }

    @ModelAttribute("message")
    public MessageDto messageDto(){
        return new MessageDto();
    }

    @GetMapping("/lecturer_message_send")
    public String sendMessageGet(Model model){

        List<UserDto> userList = userService.findAll();
        model.addAttribute("user_list", userList);

        return "lecturer/lecturer_message_send";
    }

    @PostMapping("/lecturer_message_send")
    public String sendMessagePost(
            Model model,
            @Valid @ModelAttribute("message") MessageDto messageDto,
            BindingResult errors,
            Principal principal){

        List<UserDto> userList = userService.findAll();
        model.addAttribute("user_list", userList);

        if (errors.hasErrors()) {
            return "redirect:/lecturer_message_send";
        }
        messageService.save(messageMapper.setMessageSenderAndCurrentDate(principal,messageDto));
        return "redirect:/lecturer/lecturer_message_list_sent";
    }

    @GetMapping("/lecturer_message_list_received")
    public String messageListReceived(Principal principal,Model model){
        model.addAttribute("messageList",messageService.findMessagesByReceiverUserMail(principal.getName()));
        return "lecturer/lecturer_message_list_received";
    }

    @GetMapping("/lecturer_message_list_sent")
    public String messageListSent(Principal principal,Model model){
        model.addAttribute("messageList",messageService.findMessagesBySenderUserMail(principal.getName()));
        return "lecturer/lecturer_message_list_sent";
    }

    @GetMapping("/lecturer_message_view/{receiverMail}/{messageId}")
    public String studentMessageView(@PathVariable Long messageId, @PathVariable String receiverMail, Principal principal, Model model){
        model.addAttribute("userPrincipal",userService.findUserByEmail(principal.getName()));
        model.addAttribute("userAccesing",userService.findUserByEmail(receiverMail));
        model.addAttribute("message",messageService.findMessageById(messageId));
        return "lecturer/lecturer_message_view";
    }

}
