package pl.sdacademy.college.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.sdacademy.college.component.MessageMapper;
import pl.sdacademy.college.component.StudentGroupMapper;
import pl.sdacademy.college.component.UserMapper;
import pl.sdacademy.college.dto.MessageDto;
import pl.sdacademy.college.dto.StudentGroupDto;
import pl.sdacademy.college.dto.StudentIndexDto;
import pl.sdacademy.college.dto.UserDto;
import pl.sdacademy.college.entity.Role;
import pl.sdacademy.college.service.MessageService;
import pl.sdacademy.college.service.StudentIndexService;
import pl.sdacademy.college.service.SubjectService;
import pl.sdacademy.college.service.UserService;

import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Controller
@RequestMapping("/student")
public class StudentController {

    private final UserService userService;
    private final StudentIndexService studentIndexService;
    private final StudentGroupMapper studentGroupMapper;
    private final SubjectService subjectService;
    private final MessageService messageService;
    private final UserMapper userMapper;
    private final MessageMapper messageMapper;

    private String pathReceiverMail;

    @GetMapping
    public String studentForm(Model model, Principal principal) {
        model.addAttribute("name", principal.getName());
        return "student";
    }

    @GetMapping("/student_index")
    public String getStudentIndex(Model model, Principal principal){
        UserDto userDto = userService.findUserByEmail(principal.getName());
        List<StudentIndexDto> studentIndexList = studentIndexService.findByStudentId(userDto.getId());
        model.addAttribute("studentIndexList",studentIndexList);
        return "student/student_index";
    }

    @GetMapping("/group")
    public String getStudentGroup(Model model, Principal principal){
        UserDto userDto = userService.findUserByEmail(principal.getName());
        StudentGroupDto studentGroupDto = studentGroupMapper.map(userDto.getStudentGroup());
        model.addAttribute("studentGroup",studentGroupDto);
        return "student/student_group";
    }

    @GetMapping("/student_lecturer_details/{id}")
    public String getLecturerDetails(@PathVariable Long id, Model model){
        model.addAttribute("lecturer",userService.findUserByRoleAndId(Role.LECTURER,id));
        model.addAttribute("lecturerSubjectList",subjectService.findSubjectsByLecturerId(id));
        return "student/student_lecturer_details";
    }

    @ModelAttribute("message")
    public MessageDto messageDto(){
        return new MessageDto();
    }

    @GetMapping("/student_message_send")
    public String sendMessageGet(Model model){

        List<UserDto> userList = userService.findAll();
        model.addAttribute("user_list", userList);

        return "student/student_message_send";
    }

    @PostMapping("/student_message_send")
    public String sendMessagePost(
            Model model,
            @Valid @ModelAttribute("message") MessageDto messageDto,
            BindingResult errors,
            Principal principal){

        List<UserDto> userList = userService.findAll();
        model.addAttribute("user_list", userList);

        if (errors.hasErrors()) {
            return "redirect:/student_message_send";
        }
        messageService.save(messageMapper.setMessageSenderAndCurrentDate(principal,messageDto));
        return "redirect:/student/student_message_list_sent";
    }

    @GetMapping("/student_message_list_received")
    public String messageListReceived(Principal principal,Model model){
        model.addAttribute("messageList",messageService.findMessagesByReceiverUserMail(principal.getName()));
        return "student/student_message_list_received";
    }

    @GetMapping("/student_message_list_sent")
    public String messageListSent(Principal principal,Model model){
        model.addAttribute("messageList",messageService.findMessagesBySenderUserMail(principal.getName()));
        return "student/student_message_list_sent";
    }

    @GetMapping("/student_message_view/{receiverMail}/{messageId}")
    public String messageView(@PathVariable Long messageId,@PathVariable String receiverMail,Principal principal,Model model){
        model.addAttribute("userPrincipal",userService.findUserByEmail(principal.getName()));
        model.addAttribute("userAccesing",userService.findUserByEmail(receiverMail));
        model.addAttribute("message",messageService.findMessageById(messageId));
        return "student/student_message_view";
    }

    @GetMapping("/student_message_lecturer/{string}")
    public String sendMessagePathReceiverGet(@PathVariable String string){
        pathReceiverMail = string;
        return "student/student_message_lecturer";
    }

    @PostMapping("/student_message_lecturer")
    public String sendMessagePathReceiverPost(
            @Valid @ModelAttribute("message") MessageDto messageDto,
            Model model,
            BindingResult errors,
            Principal principal){

        if (errors.hasErrors()) {
            return "redirect:/student_message_lecturer";
        }

        messageDto.setReceiverMail(pathReceiverMail);
//        messageDto.setSender(userMapper.map(userService.findUserByEmail(principal.getName())));
//        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
//        messageDto.setSendDate(dateTimeFormatter.format(LocalDateTime.now()));
        messageService.save(messageMapper.setMessageSenderAndCurrentDate(principal,messageDto));
        return "redirect:/student/student_index";
    }

    @GetMapping("/student_subject_details/{id}")
    public String getSubjectDetails(@PathVariable Long id,Model model){
        model.addAttribute("subject",subjectService.findById(id));
        model.addAttribute("groups",subjectService.findById(id).getStudentGroups());
        return "student/student_subject_details";
    }

}
