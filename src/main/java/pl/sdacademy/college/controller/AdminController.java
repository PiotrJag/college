package pl.sdacademy.college.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.sdacademy.college.component.MessageMapper;
import pl.sdacademy.college.dto.*;
import pl.sdacademy.college.entity.Role;
import pl.sdacademy.college.entity.Subject;
import pl.sdacademy.college.service.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@RequiredArgsConstructor
@Controller
@RequestMapping("/admin")
public class AdminController {

    private final UserService userService;
    private final StudentGroupService studentGroupService;
    private final SubjectService subjectService;
    private final StudentIndexService studentIndexService;
    private final MessageMapper messageMapper;
    private final MessageService messageService;

    @GetMapping
    public String getAdmin(Model model, Principal principal) {
        model.addAttribute("name", principal.getName());
        return "admin";
    }

    @Transactional
    @GetMapping("/user_list")
    public String getUserList(Model model,
                              @RequestParam(required = false) Long deleteId,
                              Principal principal) {
        if (deleteId != null) {
            userService.delete(deleteId);
            return "redirect:/admin/user_list";
        }
        List<UserDto> users = userService.findAll();
        Role role = Role.STUDENT;
        model.addAttribute("user_list", users);
        model.addAttribute("role_student", role);
        return "admin/user_list";
    }

    @PostMapping("/user_list")
    public String adminPost() {
        return "admin/user_list";
    }

    @GetMapping("/user_edit/{id}")
    public String editUserGet(@PathVariable Long id, Model model, Principal principal) {
        if (id == null) {
            return "";
            //jakaś strona z errorem ?
        }
        UserDto user = userService.findById(id);
        model.addAttribute("user", user);
        List<Role> roleList = Stream.of(Role.values()).collect(Collectors.toList());
        model.addAttribute("role_list", roleList);
        return "admin/user_edit";
    }

    @PostMapping("/user_edit")
    public String editUserPost(@Valid @ModelAttribute("user") UserDto user,
                               BindingResult errors,
                               Principal principal) {
        if (errors.hasErrors()) {
            return "admin/user_edit";
        }
        userService.save(user);
        return "redirect:/admin/user_list";
    }

    @GetMapping("/user_add")
    public String addUserGet(Model model) {
        List<Role> roleList = Stream.of(Role.values()).collect(Collectors.toList());
        model.addAttribute("role_list", roleList);
        return "admin/user_add";
    }

    @PostMapping("/user_add")
    public String addUserPost(Model model, @Valid @ModelAttribute("user") UserDto user,
                              BindingResult errors,
                              Principal principal) {
        if (errors.hasErrors()) {
            List<Role> roleList = Stream.of(Role.values()).collect(Collectors.toList());
            model.addAttribute("role_list", roleList);

            return "admin/user_add";
        }
        userService.save(user);
        return "redirect:/admin/user_list";
    }

    @ModelAttribute("user")
    public UserDto user() {
        return new UserDto();
    }


    @Transactional
    @GetMapping("/student_group_list")
    public String getStudentGroupList(Model model,
                                      @RequestParam(required = false) Long deleteId,
                                      Principal principal) {
        if (deleteId != null) {
            studentGroupService.deleteById(deleteId);
            return "redirect:/admin/student_group_list";
        }
        model.addAttribute("studentGroupList", studentGroupService.findAll());
        return "admin/student_group_list";
    }


    @GetMapping("/student_group_edit/{id}")
    public String editStudentGroupGet(@PathVariable Long id,
                                      Model model,
                                      Principal principal) {
        if (id == null) {
            return "";
            //jakaś strona z errorem ?
        }
        StudentGroupDto studentGroupDto = studentGroupService.findById(id);
        model.addAttribute("studentGroup", studentGroupDto);
        return "admin/student_group_edit";
    }

    @PostMapping("/student_group_edit")
    public String editStudentGroup(@Valid @ModelAttribute("studentGroup") StudentGroupDto studentGroup,
                                   Model model,
                                   BindingResult errors,
                                   Principal principal) {
        if (errors.hasErrors()) {
            return "admin/student_group_edit";
        }
        studentGroupService.save(studentGroup);
        return "redirect:/admin/student_group_list";
    }

    @GetMapping("/student_group_add")
    public String addStudentGroupGet() {
        return "admin/student_group_add";
    }

    @GetMapping("/student_group_add_subject")
    public String addSubjectToGroup(@RequestParam(name = "groupId") Long groupId,
                                    Model model) {
        model.addAttribute("subjects", subjectService.findAll());
        model.addAttribute("student_group_id", groupId);
        model.addAttribute("group_subjects", studentGroupService.findById(groupId));

        return "admin/student_group_add_subject";
    }

    @PostMapping("/student_group_add")
    public String addStudentGroupPost(@Valid @ModelAttribute("studentGroup") StudentGroupDto studentGroupDto,
                                      BindingResult errors,
                                      Principal principal) {
        if (errors.hasErrors()) {
            return "admin/student_group_add";
        }
        studentGroupService.save(studentGroupDto);
        return "redirect:/admin/student_group_list";
    }

    @GetMapping("/group_add_user/{groupId}/{userId}")
    public String addGroupToUser(@PathVariable Long groupId,
                                 @PathVariable Long userId) {
        studentGroupService.addGroupToUser(userService.findUserById(userId), groupId);

        Set<Subject> subjects = studentGroupService.findGroupById(groupId).getSubjects();
        userService.createIndexForUser(userService.findUserById(userId), subjects);

        return "redirect:/admin/user_add_group?userId=" + userId;
    }

    @GetMapping("/group_remove_user/{groupId}/{userId}")
    public String removeGroupFromUser(@PathVariable Long groupId,
                                      @PathVariable Long userId) {
        studentGroupService.removeGroupFromUser(userService.findUserById(userId), groupId);

        Set<Subject> subjects = studentGroupService.findGroupById(groupId).getSubjects();
        userService.removeIndexFromUser(userService.findUserById(userId), subjects);

        return "redirect:/admin/user_add_group?userId=" + userId;
    }

    @ModelAttribute("studentGroup")
    public StudentGroupDto studentGroup() {
        return new StudentGroupDto();
    }

    @Transactional
    @GetMapping("/subject_add_lecturer")
    public String addLecturersToSubjects(@RequestParam Long subjectId,Model model){
        model.addAttribute("lecturers",userService.findUsersByRole(Role.LECTURER));
        model.addAttribute("subject",subjectService.findById(subjectId));
        model.addAttribute("subjectId",subjectId);
        return "admin/subject_add_lecturer";
    }

    @GetMapping("/subject_add_lecturer/{subjectId}/{lecturerId}")
    public String addLecturerToSubject(@PathVariable Long subjectId,
                                       @PathVariable Long lecturerId){
        subjectService.addLecturerToSubject(subjectId,userService.findUserById(lecturerId));
        return "redirect:/admin/subject_list";
    }

    @GetMapping("/subject_remove_lecturer/{subjectId}/{lecturerId}")
    public String removeLecturerFromSubject(@PathVariable Long subjectId,
                                            @PathVariable Long lecturerId){
        subjectService.removeLecturerFromSubject(subjectId,userService.findUserById(lecturerId));
        return "redirect:/admin/subject_add_lecturer?subjectId="+subjectId;
    }

    @Transactional
    @GetMapping("/subject_list")
    public String getSubjectList(Model model,
                                 @RequestParam(required = false) Long deleteId,
                                 Principal principal) {
        if (deleteId != null) {
            subjectService.deleteById(deleteId);
        }

        model.addAttribute("subjectList", subjectService.findAll());
        return "admin/subject_list";
    }

    @GetMapping("/subject_edit/{id}")
    public String editSubjectGet(@PathVariable Long id,
                                 Model model,
                                 Principal principal) {
        if (id == null) {
            return "";
            //jakaś strona z errorem ?
        }
        SubjectDto subjectDto = subjectService.findById(id);
        model.addAttribute("subject", subjectDto);
        model.addAttribute("lecturers",userService.findUsersByRole(Role.LECTURER));
        return "admin/subject_edit";
    }

    @PostMapping("/subject_edit")
    public String editSubject(@Valid @ModelAttribute("subject") SubjectDto subjectDto,
                              Model model,
                              BindingResult errors,
                              Principal principal) {
        if (errors.hasErrors()) {
            return "admin/subject_edit";
        }
        subjectService.save(subjectDto);
        return "admin/subject_edit";
    }

    @ModelAttribute("subject")
    public SubjectDto subject() {
        return new SubjectDto();
    }

    @GetMapping("/subject_add")
    public String addSubjectGet() {
        return "admin/subject_add";
    }

    @PostMapping("/subject_add")
    public String addSubject(@Valid @ModelAttribute("subject") SubjectDto subjectDto,
                             BindingResult errors,
                             Principal principal) {
        if (errors.hasErrors()) {
            return "admin/subject_add";
        }
        subjectService.save(subjectDto);
        return "redirect:/admin/subject_list";
    }

    @GetMapping("/subject_add_group/{subjectId}/{groupId}")
    public String addSubjectToGroup(@PathVariable Long subjectId,
                                    @PathVariable Long groupId) {
        subjectService.addSubjectToGroup(studentGroupService.findGroupById(groupId), subjectId);

        return "redirect:/admin/student_group_add_subject?groupId=" + groupId;
    }

    @GetMapping("/subject_remove_group/{subjectId}/{groupId}")
    public String removeSubjectFromGroup(@PathVariable Long subjectId,
                                         @PathVariable Long groupId) {
        subjectService.removeSubjectFromGroup(studentGroupService.findGroupById(groupId), subjectId);

        return "redirect:/admin/student_group_add_subject?groupId=" + groupId;
    }

    @GetMapping("/user_add_group")
    public String addUserToGroup(@RequestParam(name = "userId") Long userId,
                                    Model model) {
        model.addAttribute("student_groups", studentGroupService.findAll());
        model.addAttribute("user_id", userId);
        model.addAttribute("user_dto", userService.findById(userId));

        return "admin/user_add_group";
    }

    @Transactional
    @GetMapping("/student_index_list")
    public String getStudentIndexList(Model model,
                                      @RequestParam(required = false) Long deleteId,
                                      Principal principal){
        if(deleteId!=null){
            studentIndexService.deleteById(deleteId);
            return "redirect:/admin/student_index_list";
        }
        model.addAttribute("studentIndexList", studentIndexService.findAll());
        return "admin/student_index_list";
    }

    @GetMapping("/student_index_edit/{id}")
    public String editStudentIndexGet(@PathVariable Long id,
                                      Model model,
                                      Principal principal) {
        StudentIndexDto studentIndexDto = studentIndexService.findById(id);
        model.addAttribute("studentIndex", studentIndexDto);
        return "admin/student_index_edit";
    }

    @PostMapping("/student_index_edit")
    public String editStudentIndex(@Valid @ModelAttribute("studentIndex") StudentIndexDto studentIndexDto,
                                   Model model,
                                   BindingResult errors,
                                   Principal principal){
        if (errors.hasErrors()) {
            return "admin/student_index_edit";
        }
        studentIndexService.save(studentIndexDto);
        return "admin/student_index_edit";
    }

    @GetMapping("/student_index_add")
    public String addStudentIndexGet(){
        return "admin/student_index_add";
    }

    @PostMapping("/student_index_add")
    public String addStudentIndexPost(@Valid @ModelAttribute("studentIndex") StudentIndexDto studentIndexDto,
                                      BindingResult errors,
                                      Principal principal){
        if (errors.hasErrors()) {
            return "admin/student_index_add";
        }
        studentIndexService.save(studentIndexDto);
        return "redirect:/admin/student_index_list";
    }

    @ModelAttribute("studentIndex")
    public StudentIndexDto studentIndex(){
        return new StudentIndexDto();
    }

    @ModelAttribute("message")
    public MessageDto messageDto(){
        return new MessageDto();
    }

    @GetMapping("/admin_message_send")
    public String sendMessageGet(Model model){

        List<UserDto> userList = userService.findAll();
        model.addAttribute("user_list", userList);

        return "admin/admin_message_send";
    }

    @PostMapping("/admin_message_send")
    public String sendMessagePost(
            Model model,
            @Valid @ModelAttribute("message") MessageDto messageDto,
            BindingResult errors,
            Principal principal){

        List<UserDto> userList = userService.findAll();
        model.addAttribute("user_list", userList);

        if (errors.hasErrors()) {
            return "redirect:/admin_message_send";
        }
        messageService.save(messageMapper.setMessageSenderAndCurrentDate(principal,messageDto));
        return "redirect:/admin/admin_message_list_sent";
    }

    @GetMapping("/admin_message_list_received")
    public String messageListReceived(Principal principal,Model model){
        model.addAttribute("messageList",messageService.findMessagesByReceiverUserMail(principal.getName()));
        return "admin/admin_message_list_received";
    }

    @GetMapping("/admin_message_list_sent")
    public String messageListSent(Principal principal,Model model){
        model.addAttribute("messageList",messageService.findMessagesBySenderUserMail(principal.getName()));
        return "admin/admin_message_list_sent";
    }

    @GetMapping("/admin_message_view/{receiverMail}/{messageId}")
    public String studentMessageView(@PathVariable Long messageId,@PathVariable String receiverMail,Principal principal,Model model){
        model.addAttribute("userPrincipal",userService.findUserByEmail(principal.getName()));
        model.addAttribute("userAccesing",userService.findUserByEmail(receiverMail));
        model.addAttribute("message",messageService.findMessageById(messageId));
        return "admin/admin_message_view";
    }

}
