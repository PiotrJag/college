package pl.sdacademy.college.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import pl.sdacademy.college.dto.UserDto;
import pl.sdacademy.college.service.UserService;

@RequiredArgsConstructor
@Controller
public class MainController {

    private final UserService userService;

    @GetMapping("/login")
    public String loginForm() {
        return "login";
    }

    @GetMapping({"/","/index"})
    public String indexForm() {
        return "index";
    }

    @ModelAttribute("user")
    public UserDto user() {
        return new UserDto();
    }
}
