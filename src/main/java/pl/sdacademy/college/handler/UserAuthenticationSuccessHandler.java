package pl.sdacademy.college.handler;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

@Component
public class UserAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest arg0, HttpServletResponse arg1,
                                        Authentication authentication) throws IOException, ServletException {

        boolean hasAdminRole = false;
        boolean hasStudentRole = false;
        boolean hasLecturerRole = false;
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        for (GrantedAuthority grantedAuthority : authorities) {
            if (grantedAuthority.getAuthority().equals("ROLE_ADMIN")) {
                hasAdminRole = true;
                break;
            } else if (grantedAuthority.getAuthority().equals("ROLE_STUDENT")) {
                hasStudentRole = true;
                break;
            } else if (grantedAuthority.getAuthority().equals("ROLE_LECTURER")) {
                hasLecturerRole = true;
                break;
            }
        }

        if (hasAdminRole) {
            redirectStrategy.sendRedirect(arg0, arg1, "/admin");
        } else if (hasStudentRole) {
            redirectStrategy.sendRedirect(arg0, arg1, "/student");
        } else if (hasLecturerRole) {
            redirectStrategy.sendRedirect(arg0, arg1, "/lecturer");
        } else {
            throw new IllegalStateException();
        }
    }

}
