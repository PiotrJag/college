package pl.sdacademy.college.exception;

public class EntityNotFoundByMailException extends RuntimeException {

    public EntityNotFoundByMailException(Class entityClass,String mail){
        super("Entity of class " + entityClass.getSimpleName() + " with mail " + mail + " not found");
    }
}
