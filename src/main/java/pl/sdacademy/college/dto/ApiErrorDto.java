package pl.sdacademy.college.dto;

import lombok.Data;

@Data
public class ApiErrorDto {

    public String exceptionClass;
    public String message;
}
