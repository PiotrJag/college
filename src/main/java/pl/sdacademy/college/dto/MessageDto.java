package pl.sdacademy.college.dto;

import lombok.Data;
import pl.sdacademy.college.entity.User;

@Data
public class MessageDto {

    private Long id;

    private User sender;

    private String receiverMail;

    private String text;

    private String sendDate;
}
