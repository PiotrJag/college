package pl.sdacademy.college.dto;

import lombok.Data;
import pl.sdacademy.college.entity.StudentGroup;
import pl.sdacademy.college.entity.Subject;
import pl.sdacademy.college.entity.User;

import java.time.LocalDate;

@Data
public class LectureDto {

    private Long id;

    private StudentGroup studentGroup;

    private Subject subject;

    private User user;

    private LocalDate startDate;

    private LocalDate endDate;


}
