package pl.sdacademy.college.dto;

import lombok.Data;
import pl.sdacademy.college.entity.Role;
import pl.sdacademy.college.entity.StudentGroup;
import pl.sdacademy.college.entity.StudentIndex;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
public class UserDto {

    private Long id;

    @NotBlank
    @Size(min = 3, max = 255)
    private String password;

    @Size(max = 255)
    private String name;

    @Size(max = 255)
    private String surname;

    @Email
    @NotBlank
    private String email;

    @NotNull
    private Role role;

    private StudentGroup studentGroup;

    @Size(max = 255)
    private String country;

    @Size(max = 255)
    private String postCode;

    @Size(max = 255)
    private String city;

    @Size(max = 255)
    private String street;

    private Set<StudentIndexDto> studentIndex;

}
