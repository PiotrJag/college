package pl.sdacademy.college.dto;

import lombok.Data;
import pl.sdacademy.college.entity.StudentGroup;
import pl.sdacademy.college.entity.Subject;
import pl.sdacademy.college.entity.User;

import javax.validation.constraints.NotNull;

@Data
public class StudentIndexDto {

    private Long id;

    private Subject subject;

    private User lecturer;

    private User student;

    private String grade;

    private StudentGroup studentGroup;

}
