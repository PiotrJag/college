package pl.sdacademy.college.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.sdacademy.college.entity.StudentIndex;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class StudentGradesDto {
    private List<StudentGradeDto> grades;

    public StudentGradesDto(List<StudentIndex> groupDtoList) {
        grades = new ArrayList<>();
        for (int i = 0; i < groupDtoList.size(); i++) {
            grades.add(new StudentGradeDto(groupDtoList.get(i).getId()));
        }
    }
}
