package pl.sdacademy.college.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentGradeDto {
    private String grade;
    private Long studentIndex;

    public StudentGradeDto(Long studentIndex) {
        this.studentIndex = studentIndex;
    }
}
