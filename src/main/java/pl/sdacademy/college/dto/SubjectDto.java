package pl.sdacademy.college.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import pl.sdacademy.college.entity.StudentGroup;
import pl.sdacademy.college.entity.User;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
public class SubjectDto {

    private Long id;

    @Size(max = 255)
    @NotBlank
    private String name;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<StudentGroup> studentGroups;

    private User lecturer;
}
