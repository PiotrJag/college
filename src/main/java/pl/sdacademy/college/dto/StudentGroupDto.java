package pl.sdacademy.college.dto;

import lombok.Data;
import pl.sdacademy.college.entity.Subject;
import pl.sdacademy.college.entity.User;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Set;

@Data
public class StudentGroupDto {

   private Long id;

   @Size(max = 255)
   @NotBlank
   private String name;

   @Size(max = 255)
   private String degree;

   @Size(max = 255)
   private String courseType;

   private String year;

   private Set<SubjectDto> subjects;

   private Set<UserDto> users;
}
