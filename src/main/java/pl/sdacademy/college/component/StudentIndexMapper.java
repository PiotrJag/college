package pl.sdacademy.college.component;

import org.springframework.stereotype.Component;
import pl.sdacademy.college.dto.StudentIndexDto;
import pl.sdacademy.college.entity.StudentIndex;

@Component
public class StudentIndexMapper {

    public StudentIndex map(StudentIndexDto dto){
        StudentIndex entity = new StudentIndex();
        entity.setId(dto.getId());
        entity.setGrade(dto.getGrade());
        entity.setLecturer(dto.getLecturer());
        entity.setSubject(dto.getSubject());
        entity.setStudent(dto.getStudent());
        entity.setStudentGroup(dto.getStudentGroup());
        return entity;
    }

    public StudentIndexDto map(StudentIndex entity){
        StudentIndexDto dto = new StudentIndexDto();
        dto.setId(entity.getId());
        dto.setGrade(entity.getGrade());
        dto.setLecturer(entity.getLecturer());
        dto.setSubject(entity.getSubject());
        dto.setStudent(entity.getStudent());
        dto.setStudentGroup(entity.getStudentGroup());
        return dto;
    }

}
