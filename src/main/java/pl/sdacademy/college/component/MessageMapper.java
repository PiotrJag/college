package pl.sdacademy.college.component;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.sdacademy.college.dto.MessageDto;
import pl.sdacademy.college.entity.Message;
import pl.sdacademy.college.service.UserService;

import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
@RequiredArgsConstructor
public class MessageMapper {

    private final UserMapper userMapper;
    private final UserService userService;

    public Message map(MessageDto dto){
        Message entity = new Message();
        entity.setReceiverMail(dto.getReceiverMail());
        entity.setSender(dto.getSender());
        entity.setId(dto.getId());
        entity.setText(dto.getText());
        entity.setSendDate(dto.getSendDate());
        return entity;
    }

    public MessageDto map(Message entity){
        MessageDto dto = new MessageDto();
        dto.setId(entity.getId());
        dto.setSender(entity.getSender());
        dto.setReceiverMail(entity.getReceiverMail( ));
        dto.setText(entity.getText());
        dto.setSendDate(entity.getSendDate());
        return dto;
    }

    public MessageDto setMessageSenderAndCurrentDate(Principal principal, MessageDto messageDto) {
        messageDto.setSender(userMapper.map(userService.findUserByEmail(principal.getName())));
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        messageDto.setSendDate(dateTimeFormatter.format(LocalDateTime.now()));
        return messageDto;
    }

}
