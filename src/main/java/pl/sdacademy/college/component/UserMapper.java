package pl.sdacademy.college.component;

import org.springframework.stereotype.Component;
import pl.sdacademy.college.dto.UserDto;
import pl.sdacademy.college.entity.User;

@Component
public class UserMapper {

    public User map(UserDto dto) {
        User entity = new User();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setSurname(dto.getSurname());
        entity.setEmail(dto.getEmail());
        entity.setPassword(dto.getPassword());
        entity.setRole(dto.getRole());
        entity.setCountry(dto.getCountry());
        entity.setPostCode(dto.getPostCode());
        entity.setCity(dto.getCity());
        entity.setStreet(dto.getStreet());
        entity.setStudentGroup(dto.getStudentGroup());
        return entity;
    }

    public UserDto map(User entity) {
        UserDto dto = new UserDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setSurname(entity.getSurname());
        dto.setEmail(entity.getEmail());
        dto.setPassword(entity.getPassword());
        dto.setRole(entity.getRole());
        dto.setCountry(entity.getCountry());
        dto.setPostCode(entity.getPostCode());
        dto.setCity(entity.getCity());
        dto.setStreet(entity.getStreet());
        dto.setStudentGroup(entity.getStudentGroup());
        return dto;
    }
}
