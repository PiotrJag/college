package pl.sdacademy.college.component;

import org.springframework.stereotype.Component;
import pl.sdacademy.college.dto.SubjectDto;
import pl.sdacademy.college.entity.Subject;

@Component
public class SubjectMapper {

    public Subject map(SubjectDto dto){
        Subject entity = new Subject();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setStudentGroups(dto.getStudentGroups());
        entity.setLecturer(dto.getLecturer());
        return entity;
    }

    public SubjectDto map(Subject entity){
        SubjectDto dto = new SubjectDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setStudentGroups(entity.getStudentGroups());
        dto.setLecturer(entity.getLecturer());
        return dto;
    }
}
