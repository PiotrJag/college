package pl.sdacademy.college.component;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.sdacademy.college.dto.StudentGroupDto;
import pl.sdacademy.college.entity.StudentGroup;

import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class StudentGroupMapper {

    private final SubjectMapper subjectMapper;
    private final UserMapper userMapper;

    public StudentGroup map(StudentGroupDto dto) {
        StudentGroup entity = new StudentGroup();
        entity.setId(dto.getId());
        entity.setCourseType(dto.getCourseType());
        entity.setDegree(dto.getDegree());
        entity.setName(dto.getName());
        entity.setYear(dto.getYear());
//        entity.setSubjects(dto.getSubjects().stream().map(subjectMapper::map).collect(Collectors.toSet()));
//        entity.setUsers(dto.getUsers().stream().map(userMapper::map).collect(Collectors.toSet()));
        return entity;
    }

    public StudentGroupDto map(StudentGroup entity) {
        StudentGroupDto dto = new StudentGroupDto();
        dto.setId(entity.getId());
        dto.setDegree(entity.getDegree());
        dto.setName(entity.getName());
        dto.setYear(entity.getYear());
        dto.setSubjects(entity.getSubjects().stream().map(subjectMapper::map).collect(Collectors.toSet()));
        dto.setCourseType(entity.getCourseType());
        dto.setUsers(entity.getUsers().stream().map(userMapper::map).collect(Collectors.toSet()));
        return dto;
    }
}
