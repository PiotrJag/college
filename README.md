﻿# College
This application allows you to manage the College. 
We have three groups of users: administrators, lecturers and students. 
Based on the assigned role, the user is automatically logged into his portal and can manage 
his functions.

Administrator:

* manages users, subjects, groups of students
* assigns lecturers to subjects
* assigns subjects to a group of students
* assigns groups to students

Lecturer:

* makes index entries

Student:

* Student can view his index

The system has a communicator for sending messages between all users.

Examples of basic data for the database are in the data.sql file.
Based on this data, you can log in to the system.

Login / password:

* admin@gmail.com / admin
* student@gmail.com / student
* lecturer@gmail.com / lecturer

## Screenshots
![](./screenshots/screen0.png)

![](./screenshots/screen1.png)

## Technologies

* Java,
* Spring,
* MySQL,
* [Maven](https://maven.apache.org/)

## About the authors

**Krystian Walczak and Piotr Jagodziński**

* [krystian.walczak94@gmail.com](mailto:krystian.walczak94@gmail.com)
* [piotrjagod@poczta.onet.pl](mailto:piotrjagod@poczta.onet.pl)

## License
[MIT](https://opensource.org/licenses/mit-license.php)